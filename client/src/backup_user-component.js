// @flow

//Begynte på en innloggingsfunksjonalitet, men rakk ikke ferdigstille

import * as React from 'react';
import { Component } from 'react-simplified';
import { Alert, Card, Row, Column, Form, Button, NavBar } from './widgets';
import { NavLink } from 'react-router-dom';
import loginService, { type Bruker } from './login-service';
import { createHashHistory } from 'history';

const history = createHashHistory();

export class Register extends Component {
  username = '';
  password = '';

  render() {
    return (
      <>
        <Card title="Registrer en bruker">
          <Row>
            <Column width={1}>
              <Form.Label>Brukernavn:</Form.Label>
            </Column>
            <Column width={2}>
              <Form.Input
                type="text"
                value={this.username}
                onChange={(event) => (this.username = event.currentTarget.value)}
              />
            </Column>
          </Row>
          <Row>
            <Column width={1}>
              <Form.Label>Passord:</Form.Label>
            </Column>
            <Column width={2}>
              <Form.Input
                type="password"
                value={this.password}
                onChange={(event) => (this.password = event.currentTarget.value)}
              />
            </Column>
          </Row>
        </Card>
        <Button.Success
          onClick={() => {
            loginService
              .create(this.username, this.password)
              .then((id) => history.push('/login'))
              .catch((error: Error) => Alert.danger('Error creating user: ' + error.message));
          }}
        >Lag bruker
        </Button.Success>
      </>
    );
  }
}

export class Login extends Component {
  username = '';
  password = '';
  render() {
    return (
      <>
        <Card title="Logg inn med ditt brukernavn og passord">
          <Row>
            <Column width={1}>
              <Form.Label>Brukernavn:</Form.Label>
            </Column>
            <Column width={2}>
              <Form.Input
                type="text"
                value={this.username}
                onChange={(event) => (this.username = event.currentTarget.value)}
              />
            </Column>
          </Row>
          <Row>
            <Column width={1}>
              <Form.Label>Passord:</Form.Label>
            </Column>
            <Column width={2}>
              <Form.Input
                type="password"
                value={this.password}
                onChange={(event) => (this.password = event.currentTarget.value)}
              />
            </Column>
          </Row>
        </Card>
        <Button.Success
        >Logg inn
        </Button.Success>
      </>
    );
  }
}