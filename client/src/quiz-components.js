// @flow

import * as React from 'react';
import { Component } from 'react-simplified';
import { Alert, Card, Row, Column, Form, Button, NavBar } from './widgets';
import { NavLink } from 'react-router-dom';
import quizService, { type Quiz } from './quiz-service';
import { createHashHistory } from 'history';

const history = createHashHistory();

export class Historie extends Component {
  quizs: Quiz[] = [];
  sok: string = '';

  render() {
    return (
      <>
        <Card title="Søk innenfor kategorien:">
        <Row>
          <Column width={3}>
            <Form.Input type="text" list="quizzerListe" value={this.sok} onChange={(e) => (this.sok = e.currentTarget.value)} placeholder="Søk etter quiz"></Form.Input>
            <datalist id="quizzerListe">
             {this.quizs.map((quiz) => (
               <option key={quiz.id} value={quiz.tittel}></option>
             ))}
            </datalist>
          </Column>
          <Column>
            <Button.Light 
              type="button"
              onClick={() => {
                const quiz = this.quizs.find((quiz) => quiz.tittel == this.sok);
                if (quiz) history.push('/quiz/' + quiz.tittel + '/spm');
                else Alert.danger('Quiz ikke funnet');
              }}
            >
            Finn quiz
            </Button.Light>
          </Column>
        </Row>
        </Card>
        <Card title="Quizutvalg:">
          {this.quizs.map((quiz) => (
            <Row key={quiz.id}>
              <Column>
                <NavLink to={'/quiz/' + quiz.tittel + '/spm'}>{quiz.tittel}</NavLink>
              </Column>
            </Row>
          ))}
          <br></br>
          <Button.Success onClick={() => history.push('/quizny')}>Ny quiz</Button.Success>
        </Card>
      </>
    );
  }

  mounted() {
    quizService
      .getHistorie()
      .then((quizs) => (this.quizs = quizs))
      .catch((error: Error) => Alert.danger('Error getting tasks: ' + error.message));
  }
}

export class Sport extends Component {
  quizs: Quiz[] = [];
  sok: string = '';

  render() {
    return (
      <>
        <Card title="Søk innenfor kategorien:">
        <Row>
          <Column width={3}>
            <Form.Input type="text" list="quizzerListe" value={this.sok} onChange={(e) => (this.sok = e.currentTarget.value)} placeholder="Søk etter quiz"></Form.Input>
            <datalist id="quizzerListe">
             {this.quizs.map((quiz) => (
               <option key={quiz.id} value={quiz.tittel}></option>
             ))}
            </datalist>
          </Column>
          <Column>
            <Button.Light 
              type="button"
              onClick={() => {
                const quiz = this.quizs.find((quiz) => quiz.tittel == this.sok);
                if (quiz) history.push('/quiz/' + quiz.tittel + '/spm');
                else Alert.danger('Quiz ikke funnet');
              }}
            >
            Finn quiz
            </Button.Light>
          </Column>
        </Row>
        </Card>
        <Card title="Quizutvalg:">
          {this.quizs.map((quiz) => (
            <Row key={quiz.id}>
              <Column>
                <NavLink to={'/quiz/' + quiz.tittel + '/spm'}>{quiz.tittel}</NavLink>
              </Column>
            </Row>
          ))}
          <br></br>
          <Button.Success onClick={() => history.push('/quizny')}>Ny quiz</Button.Success>
        </Card>
      </>
    );
  }

  mounted() {
    quizService
      .getSport()
      .then((quizs) => (this.quizs = quizs))
      .catch((error: Error) => Alert.danger('Error getting tasks: ' + error.message));
  }
}

export class Naturogvitenskap extends Component {
  quizs: Quiz[] = [];
  sok: string = '';

  render() {
    return (
      <>
        <Card title="Søk innenfor kategorien:">
        <Row>
          <Column width={3}>
            <Form.Input type="text" list="quizzerListe" value={this.sok} onChange={(e) => (this.sok = e.currentTarget.value)} placeholder="Søk etter quiz"></Form.Input>
            <datalist id="quizzerListe">
             {this.quizs.map((quiz) => (
               <option key={quiz.id} value={quiz.tittel}></option>
             ))}
            </datalist>
          </Column>
          <Column>
            <Button.Light 
              type="button"
              onClick={() => {
                const quiz = this.quizs.find((quiz) => quiz.tittel == this.sok);
                if (quiz) history.push('/quiz/' + quiz.tittel + '/spm');
                else Alert.danger('Quiz ikke funnet');
              }}
            >
            Finn quiz
            </Button.Light>
          </Column>
        </Row>
        </Card>
        <Card title="Quizutvalg:">
          {this.quizs.map((quiz) => (
            <Row key={quiz.id}>
              <Column>
                <NavLink to={'/quiz/' + quiz.tittel + '/spm'}>{quiz.tittel}</NavLink>
              </Column>
            </Row>
          ))}
          <br></br>
          <Button.Success onClick={() => history.push('/quizny')}>Ny quiz</Button.Success>
        </Card>
      </>
    );
  }

  mounted() {
    quizService
      .getNatur()
      .then((quizs) => (this.quizs = quizs))
      .catch((error: Error) => Alert.danger('Error getting tasks: ' + error.message));
  }
}

export class Samfunnogdagligliv extends Component {
  quizs: Quiz[] = [];
  sok: string = '';
  render() {
    return (
      <>
        <Card title="Søk innenfor kategorien:">
        <Row>
          <Column width={3}>
            <Form.Input type="text" list="quizzerListe" value={this.sok} onChange={(e) => (this.sok = e.currentTarget.value)} placeholder="Søk etter quiz"></Form.Input>
            <datalist id="quizzerListe">
             {this.quizs.map((quiz) => (
               <option key={quiz.id} value={quiz.tittel}></option>
             ))}
            </datalist>
          </Column>
          <Column>
            <Button.Light 
              type="button"
              onClick={() => {
                const quiz = this.quizs.find((quiz) => quiz.tittel == this.sok);
                if (quiz) history.push('/quiz/' + quiz.tittel + '/spm');
                else Alert.danger('Quiz ikke funnet');
              }}
            >
            Finn quiz
            </Button.Light>
          </Column>
        </Row>
        </Card>
        <Card title="Quizutvalg:">
          {this.quizs.map((quiz) => (
            <Row key={quiz.id}>
              <Column>
                <NavLink id={quiz.id} to={'/quiz/' + quiz.tittel + '/spm'}>{quiz.tittel}</NavLink>
              </Column>
            </Row>
          ))}
          <br></br>
          <Button.Success onClick={() => history.push('/quizny')}>Ny quiz</Button.Success>
        </Card>
      </>
    );
  }

  mounted() {
    quizService
      .getSamfunn()
      .then((quizs) => (this.quizs = quizs))
      .catch((error: Error) => Alert.danger('Error getting tasks: ' + error.message));
  }
}

//Opprette ny quiz
export class QuizNy extends Component {
  quizId = 1;
  tittel = '';
  kategori = '';
  spm = '';
  a = '';
  b = '';
  c = '';
  riktig_svar = '';

  render() {
    return (
      <>
        <Card title="Ny quiz">
          <Row>
            <Column width={1.5}>
              <Form.Label>Tittel:</Form.Label>
            </Column>
            <Column width={3}>
              <Form.Input
                type="text"
                id="tittel"
                value={this.tittel}
                onChange={(event) => (this.tittel = event.currentTarget.value)}
              />
            </Column>
          </Row>
          <Row>
            <Column width={1.5}>Kategori:</Column>
            <Column width={2}>
              <Form.Select
              //Velg hvilken kategori som quizen tilhører
                id="kategori"
                defaultValue={'DEFAULT'}
                onChange={(event) => (this.kategori = event.currentTarget.value)}>
                <option value="DEFAULT" disabled>Velg en kategori</option>
                <option value="Historie">Historie</option>
                <option value="Sport">Sport</option>
                <option value="Natur">Natur og vitenskap</option>
                <option value="Samfunn">Samfunn og dagligliv</option>
              </Form.Select>
            </Column>
          </Row>
          <Row>
            <Column width={1.5}>
              <Form.Label>Spørsmål:</Form.Label>
            </Column>
            <Column width={3}>
              <Form.Input
              //Skriv inn et spørsmål
                type="text"
                id="spm"
                value={this.spm}
                onChange={(event) => (this.spm = event.currentTarget.value)}
              />
            </Column>
          </Row>
          <Row></Row>
          <Row>
            <Column width={1.5}>Valg a:</Column>
            <Column width={2}>
              <Form.Input
              //På de tre neste text feltene skriver du svar alternativer
                type="text"
                id="a"
                value={this.a}
                onChange={(event) => (this.a = event.currentTarget.value)}
              />
            </Column>
            <Column width={1.5}>Valg b:</Column>
            <Column width={2}>
              <Form.Input
                type="text"
                id="b"
                value={this.b}
                onChange={(event) => (this.b = event.currentTarget.value)}
              />
            </Column>
            <Column width={1.5}>Valg c:</Column>
            <Column width={2}>
              <Form.Input
                type="text"
                id="c"
                value={this.c}
                onChange={(event) => (this.c = event.currentTarget.value)}
              />
            </Column>
          </Row>
          <Row>
            <Column width={1.5}>Riktig valg:</Column>
            <Column width={2}>
              <Form.Select
              //Velger hvilken av alternativene som er riktig. Riktig svar peker på tilhørende bokstav fra database kollonen
                id="riktig_svar"
                defaultValue={'DEFAULT'}
                onChange={(event) => (this.riktig_svar = event.currentTarget.value)}>
                <option value="DEFAULT" disabled>Velg riktig valg</option>
                <option value="a">Valg a</option>
                <option value="b">Valg b</option>
                <option value="c">Valg c</option>
              </Form.Select>
            </Column>
          </Row>
          <br></br>
          <Row>
            <Column>
              <Button.Success
                onClick={() => {
                  quizService
                    .create(this.tittel, this.kategori, this.spm, this.a, this.b, this.c, this.riktig_svar)
                    .then(() => history.push(`/quizny/laget`))
                    .catch((error: Error) => Alert.danger('Vennligst fyll ut alle felter og kategori/riktig valg: ' + error.message));
                }
                }
              >
                Opprett
              </Button.Success>
            </Column>
          </Row>
        </Card>
      </>
    );
  }
}

export class QuizLaget extends Component {
  render() {
    return (
      <>
        <Card title="Gratulerer! Du har opprettet en ny quiz"></Card>
      </>
    );
  }
}