// @flow

import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import { HashRouter, Route } from 'react-router-dom';
import { NavBar, Card, Alert, Button, Column, Row, Form } from './widgets';
import { Historie, Sport, Naturogvitenskap, Samfunnogdagligliv, QuizNy, QuizLaget } from './quiz-components';
import { QuizDetaljer } from './quiz-spill';
import { QuizSpm, QuizRediger, QuizSlettet } from './quiz-rediger';
import { createHashHistory } from 'history';
import quizService, { type Quiz } from './quiz-service';


const history = createHashHistory();

export class Meny extends Component {
  render() {
    return (
      <NavBar brand="Quiz">
        <NavBar.Link to="/historie">Historie</NavBar.Link>
        <NavBar.Link to="/sport">Sport</NavBar.Link>
        <NavBar.Link to="/naturogvitenskap">Natur og vitenskap</NavBar.Link>
        <NavBar.Link to="/samfunnogdagligliv">Samfunn og dagligliv</NavBar.Link>
      </NavBar>
    );
  }
}

//Forside, med mulighet for å søke seg frem blant alle quizene
export class Hjem extends Component {
  quizs: Quiz[] = [];
  sok: string = '';

  render() {
    return (
      <>
        <Card title="Velkommen til vår quiz-app!">
        <p>Dette er en spørrelek hvor du kan teste din kunnskap om en gitt kategori ved å svare på et sett med spørsmål. Målet er å få flest mulig riktige svar. De ulike kategoriene finner du i baren øverst på siden.</p>
        <img src="quiz.jpg" alt="quizTime" width="350"></img>
        </Card>
        <Card title="Søk blant quizer:">
        <Row>
          <Column width={3}>
            <Form.Input type="text" list="quizzerListe" value={this.sok} onChange={(e) => (this.sok = e.currentTarget.value)} placeholder="Søk etter quiz"></Form.Input>
            <datalist id="quizzerListe">
             {this.quizs.map((quiz) => (
               <option key={quiz.id} id={quiz.tittel} value={quiz.tittel}></option>
             ))}
            </datalist>
          </Column>
          <Column>
            <Button.Light 
              type="button"
              onClick={() => {
                const quiz = this.quizs.find((quiz) => quiz.tittel == this.sok);
                if (quiz) history.push('/quiz/' + quiz.tittel + '/spm');
                else Alert.danger('Quiz ikke funnet');
              }}
            >
            Finn quiz
            </Button.Light>
          </Column>
        </Row>
        </Card>
      </>
    );
  }
  
  mounted() {
    quizService
      .getAll()
      .then((quizs) => (this.quizs = quizs))
      .catch((error: Error) => Alert.danger('Error getting tasks: ' + error.message));
  }
}

const root = document.getElementById('root');
if (root)
  ReactDOM.render(
    <HashRouter>
      <div>
        <Alert />
        <Meny />
        <Route exact path="/" component={Hjem} />
        <Route exact path="/historie" component={Historie} />
        <Route exact path="/sport" component={Sport} />
        <Route exact path="/naturogvitenskap" component={Naturogvitenskap} />
        <Route exact path="/samfunnogdagligliv" component={Samfunnogdagligliv} />
        <Route exact path="/quiz/:tittel/spm" component={QuizDetaljer} />
        <Route exact path="/rediger/:tittel/alleSpm" component={QuizSpm} /> 
        <Route exact path="/rediger/:id(\d+)/spm" component={QuizRediger} />
        <Route exact path="/quizny" component={QuizNy} />
        <Route exact path="/quizny/laget" component={QuizLaget} />
        <Route exact path="/slettet" component={QuizSlettet} />
      </div>
    </HashRouter>,
    root
  );
