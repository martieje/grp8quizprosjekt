// @flow

import * as React from 'react';
import { Component } from 'react-simplified';
import { Alert, Card, Row, Column, Form, Button, NavBar } from './widgets';
import { NavLink } from 'react-router-dom';
import quizService, { type Quiz } from './quiz-service';
import { createHashHistory } from 'history';

const history = createHashHistory();

//Viser alle spørsmål til en valgt quiz. Trykk for å redigere spørsmålet, slett hele quizen, eller avbryt
export class QuizSpm extends Component<{ match: { params: { tittel: string } } }> {
  quizs: Quiz[] = [];

  render() {
    return (
      <>
        <Card title="Velg spørsmål">
          {this.quizs.map((quiz) => (
              <Row key={quiz.id}>
                <Column>
                  <NavLink to={'/rediger/' + quiz.id + '/spm'}>{quiz.spm}</NavLink>
                </Column>
              </Row>
            ))}
            <br></br>
            <Button.Danger 
              onClick={() => {
                quizService
                .deleteQuiz(this.props.match.params.tittel)
                .then(history.push('/slettet'))
                .catch((error: Error) => Alert.danger('Error deleting quiz: ' + error.message));
              }
            }
            >Slett Quiz
          </Button.Danger>
          <Button.Light
            onClick={() => {
              history.push('/quiz/' + this.props.match.params.tittel + '/spm')
            }}
            >Avbryt
          </Button.Light>
        </Card>
      </>
    );
  }

  mounted() {
    quizService
      .getQuiz(this.props.match.params.tittel)
      .then((quizs) => (this.quizs = quizs))
      .catch((error: Error) => Alert.danger('Error getting tasks: ' + error.message))
  }
}

//Rediger valgt spørsmål
export class QuizRediger extends Component<{ match: { params: { id: number } } }> {
    quiz: Quiz[] = [];
    render() {
      return (
        <>
          <Card title="Rediger spørsmålet">
            <Card title="Spørsmål tekst">
              <Form.Input
                type="text"
                id="spm"
                value={this.quiz.spm}
                onChange={(event) => (this.quiz.spm = event.currentTarget.value)}
                rows={2}
              />
            </Card>
            <Card title="Endre svar alternativer">
            <Row>
              <Column width={1}>
                <Form.Label>Valg 1:</Form.Label>
              </Column>
              <Column>
                <Form.Input
                  type="text"
                  id="a"
                  value={this.quiz.a}
                  onChange={(event) => (this.quiz.a = event.currentTarget.value)}
                />
              </Column>
            </Row>
            <Row>
              <Column width={1}>
                <Form.Label>Valg 2:</Form.Label>
              </Column>
              <Column>
                <Form.Input
                  type="text"
                  id="b"
                  value={this.quiz.b}
                  onChange={(event) => (this.quiz.b = event.currentTarget.value)}
                />
              </Column>
            </Row>
            <Row>
              <Column width={1}>
                <Form.Label>Valg 3:</Form.Label>
              </Column>
              <Column>
                <Form.Input
                  type="text"
                  id="c"
                  value={this.quiz.c}
                  onChange={(event) => (this.quiz.c = event.currentTarget.value)}
                />
              </Column>
            </Row>
            </Card>
            <Card title="Velg riktig svar">
              <Row>
                <Column>
                  <Form.Select
                    id="riktig_svar"
                    defaultValue={'DEFAULT'}
                    onChange={(event) => (this.quiz.riktig_svar = event.currentTarget.value)}>
                    <option value="DEFAULT" disabled>Velg riktig svar</option>
                    <option value="a">Valg a</option>
                    <option value="b">Valg b</option>
                    <option value="c">Valg c</option>
                  </Form.Select>
                </Column>
              </Row>
            </Card>
            <br></br>
            <Row>
              <Column>
                <Button.Success
                  onClick={() => {
                    quizService
                      .update(this.quiz.id, this.quiz.tittel, this.quiz.spm, this.quiz.a, this.quiz.b, this.quiz.c, this.quiz.riktig_svar)
                      .then(() => history.push('/rediger/' + this.quiz.tittel + '/alleSpm'))
                      .catch((error: Error) => Alert.danger('Feil under lagring av endringer: ' + error.message));
                  }}
                >Lagre</Button.Success>
                <Button.Light
                  onClick={() => {
                    history.push('/rediger/' + this.quiz.tittel + '/alleSpm')
                  }}
                >Avbryt</Button.Light>
              </Column>
              <Column right>
                <Button.Danger onClick={() => {
                  quizService
                    .delete(this.quiz.id)
                    .then(history.push('/rediger/' + this.quiz.tittel + '/alleSpm'))
                    .catch((error: Error) => Alert.danger('Error deleting quiz: ' + error.message));
                }}
  
                >
                  Slett
                </Button.Danger>
              </Column>
            </Row>
          </Card>
        </>
      );
    }
  
    mounted() {
      quizService
        .get(this.props.match.params.id)
        .then((quiz) => (this.quiz = quiz))
        .catch((error: Error) => Alert.danger('Feil under henting av quiz: ' + error.message));
    }
  }

  //Her havner du etter å ha slettet en hel quiz
  export class QuizSlettet extends Component {
    render() {
      return (
        <>
          <Card title="Du har nå slettet en quiz! Velg kategori for flere quizer"></Card>
        </>
      );
    }
  }