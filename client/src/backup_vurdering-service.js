// @flow

//Begynte på en vurderingsfunksjonalitet, men rakk ikke ferdigstille

import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3001/api/v2';

export type Vurdering = {
  vurdering: number;
  quizId: number;
}

class VurderingService {
  /**
   * Get quiz with given id.
   */
  get(id: number) {
    return axios.get<Vurdering>(`/quiz/${id}/gjennomfort`).then((response) => response.data);
  }

  create(vurdering: number, quizId: number ) {
    return axios
        .post<{}, { id: number }>(`/quiz/${quizId}/gjennomfort`, {vurdering: vurdering})
        .then((response) => response.data.id);
  }
}

const vurderingService = new VurderingService();
export default vurderingService;