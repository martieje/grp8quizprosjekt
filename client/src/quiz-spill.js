// @flow

import * as React from 'react';
import { Component } from 'react-simplified';
import { Alert, Card, Row, Column, Form, Button, NavBar } from './widgets';
import { NavLink } from 'react-router-dom';
import quizService, { type Quiz } from './quiz-service';
import { createHashHistory } from 'history';

const history = createHashHistory();

//En type som skal lagre svarene valgt av brukeren
export type Brukersvars = {
    brukerSvar: string,
    brukerSvarTekst: string,
    riktig: string,
  };

  //Spill deg gjennom en quiz og se din poengsum
  export class QuizDetaljer extends Component<{ match: { params: { tittel: string } } }> {
    quiz: Quiz[] = [];
    spmTeller = 0;
    bruker: Brukersvars[] = [];
    svar: string = '';
    svarTekst: string = '';
    seResultat: boolean = false;

    render() {
      if(this.quiz[0] == undefined) return 'Quiz har ikke lastet enda, eller er tom'; //er quiz array tom, vil denne teksten bli returnet
      //spmTeller økes hver gang en spørsmål blir svart. 
      //Er den lik lengden på quiz array, betyr det at alle spørsmålene er svart og quizen er ferdig.
      //Da vil det vises en knapp for å prøve igjen, og en annen som kan vise mer detaljer(fasit).
      return this.spmTeller >= this.quiz.length ? (
        <>
          <Card>
            {/* Funksjonen som sjekker om du svarte riktig, og viser resultat */}
          {this.sjekkSvar()} 
            <br></br>
            <Column>
              <Row>
                <Button.Success id="prov" center onClick={() => this.reset()}>Prøv igjen</Button.Success>
              </Row>
              <Row>
                <Button.Light
                  id="detaljer"
                  center
                  onClick={() => {
                    this.seResultat = true;
                  }}
                >
                  Detaljer
                </Button.Light>
              </Row>
            </Column>
            <br></br>
            {/* Fuknsjonen som vier detaljer. Det vil si hvilke alternativer som var riktig, og hva du svarte */}
            {this.seResultat ? this.seSvar() : ''}
          </Card>
        </>
      ) : (
        <>
        {/* spmTeller vil være 0 her. Da er man på første spørsmål */}
          <Card>
            {/* Viser et unikt bilde for kategorien quizen hører til */}
            <img src={this.quiz.[this.spmTeller].kategori + '.png'} width="200" alt={this.quiz.[this.spmTeller].kategori} />
          </Card>
          <Card>
            {this.spmTeller == 0 ? (
              <Card title="Velg riktig svar. Deretter lagrer du svaret"></Card>
            ) : (
              ''
            )}
            {/* Brukeren svarte på det første spørsmålet. Funksjonen vil deretter fortelle antall riktige  av mulige svar underveis */}
            {this.sjekkSvar()}
            <Card>
              <Row>
                <Column>{this.quiz[this.spmTeller].spm}</Column>
              </Row>
              <Row>
                <Column>
                  <input type="radio" id="valg1" name="alternativ" value="a"
                    onChange={() => { 
                      this.svar = 'a';
                      this.svarTekst = this.quiz[this.spmTeller].a;
                      }
                    }
                  ></input>
                  <label htmlFor="valg1">a: {this.quiz[this.spmTeller].a}</label>
                </Column>
              </Row>
              <Row>
                <Column>
                  <input type="radio" id="valg2" name="alternativ" value="b"
                    onChange={() => {
                      this.svar = 'b';
                      this.svarTekst = this.quiz[this.spmTeller].b;
                      }
                    }
                  ></input>
                  <label htmlFor="valg2">b: {this.quiz[this.spmTeller].b}</label>
                </Column>
              </Row>
              <Row>
                <Column>
                  <input type="radio" id="valg3" name="alternativ" value="c" 
                    onChange={() => {
                      this.svar = 'c';
                      this.svarTekst = this.quiz[this.spmTeller].c;
                      }
                    }
                  ></input>
                  <label htmlFor="valg3">c: {this.quiz[this.spmTeller].c}</label>
                </Column>
              </Row>
              <Button.Success
              id="sendsvar"
              onClick={() => { 
                if(this.svar.length == 1) {
                  //Etter brukeren har valgt et alternativ og velger 'Lagre Svar', vil svaret bli sendt til en funksjon
                  //som lagrer det i et array for brukersvar
                  this.sendSvar(this.svar, this.svarTekst);
                  }
                }
              }
              >Lagre Svar
              </Button.Success>
            </Card>
          </Card>
          <Button.Success
          //Mulighet for å redigere en quiz
            id="quizrediger"
            onClick={() => history.push('/rediger/' + this.props.match.params.tittel + '/alleSpm')} //stiovergang?
          >
            Rediger
          </Button.Success>
        </>
      );
    }
  
    mounted() {
      quizService
        .getQuiz(this.props.match.params.tittel)
        .then((spm) => (this.quiz = spm))
        .catch((error: Error) => Alert.danger('Feil under henting av quiz: ' + error.message));
    }
  
    sendSvar(svar: string, svarTekst: string) {
      this.bruker.push({
        brukerSvar: svar,
        brukerSvarTekst: svarTekst,
        riktig: this.quiz[this.spmTeller].riktig_svar,
      });
      this.spmTeller += 1;
      console.log(this.bruker[0]);
    }

    sjekkSvar = () => {
      if(this.bruker[0] == undefined) return '';
      let riktigeSvar: number = 0;
      for (let i = 0; i < this.bruker.length; i++) {
        if(this.bruker[i].riktig == this.bruker[i].brukerSvar) {
          riktigeSvar += 1;
         }
      }
  
      return (
        <Card
          title={
            'Du ' +
            (this.bruker.length >= this.quiz.length ? 'fikk ' : 'har ') +
            riktigeSvar +
            ' riktig' +
            (riktigeSvar !== 1 ? 'e' : '') +
            ' svar av ' +
            this.bruker.length +
            ' mulig' +
            (this.spmTeller > 1 ? 'e' : '') +
            '.'
          }
        ></Card>
      );
    };
    //Starten quizen på nytt når 'Prøv igjen' knappen blir aktivert
    reset = () => {
      this.spmTeller = 0;
      this.bruker = [];
      this.seResultat = false;
      this.svar = '';
    };
    
    seSvar = () => { //Funksjonen til 'Detaljer' knappen. 
      return (
        <>
          {this.quiz.map((spm, index) => (
            <div>
              <Card title={spm.spm}>
                <Card title="Riktig svar:">
                  {'Alternativ ' + spm.riktig_svar + ': ' + spm[spm.riktig_svar]}
                </Card>
                <Card 
                  title={
                    this.bruker[index].brukerSvar == this.bruker[index].riktig
                      ? "Ditt svar er riktig!"
                      : "Ditt svar er feil."
                  }
                >
                  {'Du valgte alternativ ' + this.bruker[index].brukerSvar + ': ' + this.bruker[index].brukerSvarTekst}
                </Card>
              </Card>
              <br></br>
            </div>
          ))}
        </>
      );
    };
  }