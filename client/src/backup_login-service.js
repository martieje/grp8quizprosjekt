// @flow

//Begynte på en innloggingsfunksjonalitet, men rakk ikke ferdigstille
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3001/api/v2';

export type Bruker = {
  username: string,
  password: string
};

class LoginService {

  create(username: string, password: string) {
    return axios
      .post<{}, { username: string, password: string }>(`/login`, { username: username, password: password })
      .then((response) => response.data);
  }

}

const loginService = new LoginService();
export default loginService;