// @flow
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:3001/api/v2';

export type Quiz = {
  id: number,
  tittel: string,
  kategori: string,
  spm: string,
  a: string,
  b: string,
  c: string,
  riktig_svar: string
};
class QuizService {
  /**
   * Hent quiz.
   */
  get(id: number) {
    return axios.get<Quiz>('/rediger/' + id + '/spm').then((response) => response.data);
  }
  getQuiz(tittel: string) {
    return axios.get<Quiz[]>('/quiz/' + tittel + '/spm').then((response) => response.data);
  }

  /**
   * Hent alle quizzer.
   */
  getAll() {
    return axios.get<Quiz[]>('/').then((response) => response.data);
  }
  getHistorie() {
    return axios.get<Quiz[]>('/historie').then((response) => response.data);
  }
  getSport() {
    return axios.get<Quiz[]>('/sport').then((response) => response.data);
  }
  getNatur() {
    return axios.get<Quiz[]>('/naturogvitenskap').then((response) => response.data);
  }
  getSamfunn() {
    return axios.get<Quiz[]>('/samfunnogdagligliv').then((response) => response.data);
  }

  /**
   * Opprett ny quiz
   */
  create(tittel: string, kategori: string, spm: string, a: string, b: string, c: string, riktig_svar: string) {
    return axios
      .post<{}, { id: number }>('/quizny', {tittel: tittel, kategori: kategori, spm: spm, a: a, b: b, c: c, riktig_svar: riktig_svar })
      .then((response) => response.data.id);
  }

  /**
   * Oppdater/lagre quiz
   */
  update(id: number, tittel: string, spm: string, a: string, b: string, c: string, riktig_svar: string) {
    return axios
      .put<{}, {}, {}, {}, {}, {}, {}>(`/quiz/${id}`, {
        id: id,
        tittel: tittel,
        spm: spm,
        a: a,
        b: b,
        c: c,
        riktig_svar: riktig_svar
      })
      .then((response) => response.data);
  }

  /**
  * Slett et spørsmål eller en hel quiz
  */
  delete(id: number) {
    return axios.delete<Quiz>('/rediger/' + id + '/spm').then((response) => console.log('Element deleted'));
  }
  deleteQuiz(tittel: string) {
    return axios.delete<Quiz>('/rediger/' + tittel + '/alleSpm').then((response) => console.log('Element deleted'));
  }

}

const quizService = new QuizService();
export default quizService;