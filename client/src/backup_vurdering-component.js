// @flow

//Begynte på en vurderingsfunksjonalitet, men rakk ikke ferdigstille

import * as React from 'react';
import { Component } from 'react-simplified';
import { Alert, Card, Row, Column } from './widgets';
import quizService, { type Quiz } from './quiz-service';
import vurderingService, { type Vurdering } from './vurdering-service';
import { createHashHistory } from 'history';

export class QuizGjort extends Component {
      quizs: Quiz[] = [];
      vurderinger: Vurdering[] = [];
      riktig_svar = '';
      quiz = '';

      render() {
        return (
          <>
            <Card title="Quiz gjennomført, du er flink!"></Card>
            <Card>
              <Row>
                <Column>
                  Gjennomsnittlig terningskast: {' '}
                  {this.vurderinger
                    .filter((vurdering) => vurdering.quizId == quiz.id)
                    .reduce((average, vurdering, index, vurderinger) => average + vurdering.vurdering / vurderinger.length, 0
                  )}
                </Column>
                <Column right>
                   {[1, 2, 3, 4, 5, 6].map((vurdering) => (
                      <img key={vurdering} src={vurdering + '.png'} width="30" onClick={() => this.vurderQuiz(this.quiz, vurdering)} />
                    ))}
                </Column>
              </Row>
            </Card>
          </>
        );
      }
      mounted() {
        quizService
          .get(this.props.match.params.id)
          .then((quiz) => (this.quiz = quiz))
          .catch((error: Error) => Alert.danger('Feil under henting av quiz: ' + error.message));
      }
  
      vurderQuiz(quiz, vurdering) {
        console.log('test');
        vurderingService.create(this.vurdering, this.quiz.quizId, () => this.mounted());
      }
    }