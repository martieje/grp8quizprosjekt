// @flow

import * as React from 'react';
import { Component } from 'react-simplified';
import { shallow } from 'enzyme';
import { Alert, Card, Row, Column, Button, NavBar, Form } from '../src/widgets';
import { NavLink } from 'react-router-dom';


describe('Button.Success widget tests', () => {
    test('Draws correctly', () => {
        // $FlowExpectedError: do not type check next line.
        const wrapper = shallow(<Button.Success>test</Button.Success>);

        expect(
            wrapper.matchesElement(
                <button type="button" className="btn btn-success">
                    test
        </button>
            )
        ).toEqual(true);
    });

    test('Draws correctly small', () => {
        // $FlowExpectedError: do not type check next line.
        const wrapper = shallow(<Button.Success small>test</Button.Success>);

        expect(
            wrapper.matchesElement(
                <button type="button" className="btn btn-success btn-sm py-0">
                    test
        </button>
            )
        ).toEqual(true);
    });

    test('Button calls function on click-event', () => {
        let buttonClicked = false;
        const wrapper = shallow(
            <Button.Success onClick={() => (buttonClicked = true)}>test</Button.Success>
        );

        wrapper.find('button').simulate('click');

        expect(buttonClicked).toEqual(true);
    });
});

describe('Button.Danger widget tests', () => {
    test('Draws correctly', () => {
        // $FlowExpectedError: do not type check next line.
        const wrapper = shallow(<Button.Danger>test</Button.Danger>);

        expect(
            wrapper.matchesElement(
                <button type="button" className="btn btn-danger">
                    test
        </button>
            )
        ).toEqual(true);
    });

    test('Draws correctly small', () => {
        // $FlowExpectedError: do not type check next line.
        const wrapper = shallow(<Button.Danger small>test</Button.Danger>);

        expect(
            wrapper.matchesElement(
                <button type="button" className="btn btn-danger btn-sm py-0">
                    test
        </button>
            )
        ).toEqual(true);
    });

    test('Button calls function on click-event', () => {
        let buttonClicked = false;
        const wrapper = shallow(
            <Button.Danger onClick={() => (buttonClicked = true)}>test</Button.Danger>
        );

        wrapper.find('button').simulate('click');

        expect(buttonClicked).toEqual(true);
    });
});

describe('Button.Light widget tests', () => {
    test('Draws correctly', () => {
        // $FlowExpectedError: do not type check next line.
        const wrapper = shallow(<Button.Light>test</Button.Light>);

        expect(
            wrapper.matchesElement(
                <button type="button" className="btn btn-light">
                    test
        </button>
            )
        ).toEqual(true);
    });

    test('Draws correctly small', () => {
        // $FlowExpectedError: do not type check next line.
        const wrapper = shallow(<Button.Light small>test</Button.Light>);

        expect(
            wrapper.matchesElement(
                <button type="button" className="btn btn-light btn-sm py-0">
                    test
        </button>
            )
        ).toEqual(true);
    });

    test('Button calls function on click-event', () => {
        let buttonClicked = false;
        const wrapper = shallow(
            <Button.Light onClick={() => (buttonClicked = true)}>test</Button.Light>
        );

        wrapper.find('button').simulate('click');

        expect(buttonClicked).toEqual(true);
    });
});

describe('Card widget tests', () => {
    test('Draws correctly', () => {
        const wrapper = shallow(<Card>test</Card>);

        expect(wrapper.matchesElement(
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title"></h5>
                    <div className="card-text">test</div>
                </div>
            </div>)).toEqual(true);
    });

    test('Draws correctly with title', () => {
        const wrapper = shallow(<Card title="test">test</Card>);

        expect(wrapper.matchesElement(
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title">test</h5>
                    <div className="card-text">test</div>
                </div>
            </div>)).toEqual(true);
    });
});

describe('Row widget tests', () => {
    test('Draws correctly', () => {
        const wrapper = shallow(<Row>test</Row>);

        expect(wrapper.matchesElement(<div className="row">test</div>)).toEqual(true);
    });
});

describe('Column widget tests', () => {
    test('Draws correctly', () => {
        const wrapper = shallow(<Column>test</Column>);

        expect(wrapper.matchesElement(<div className="col">test</div>)).toEqual(true);
    });

    test('Draws correctly when property width is set', () => {
        const wrapper = shallow(<Column width={2}>test</Column>);

        expect(wrapper.matchesElement(<div className="col-2">test</div>)).toEqual(true);
    });

    test('Draws correctly when property right is set', () => {
        const wrapper = shallow(<Column right>test</Column>);

        expect(wrapper.matchesElement(<div className="col text-right">test</div>)).toEqual(true);
    });
});

describe('NavBarLink widget tests', () => {
    test('Draws NavBar.Link correctly', () => {
        const wrapper = shallow(<NavBar.Link to="/test">test</NavBar.Link>);

        expect(wrapper.matchesElement(<NavLink className="nav-link" activeClassName="active" to="/test">test</NavLink>)).toEqual(true);
    });
    
    test('Draws Navbar correctly', () => {
        const wrapper = shallow(
        <NavBar brand="testBrand">
            test
        </NavBar>
        );

        expect(wrapper.matchesElement(
        <nav className="navbar navbar-expand-sm bg-light navbar-light">
            {
                <NavLink className="navbar-brand" activeClassName="active" exact to="/">
                    testBrand
                </NavLink>
            }
            <ul className="navbar-nav">
                 test
            </ul>
        </nav>
            )).toEqual(true);
    });
});

describe('Form widget tests', () => {
    test('Draws label correctly', () => {
        const wrapper = shallow(<Form.Label>test</Form.Label>);

        expect(wrapper.matchesElement(<label className="col-form-label">test</label>)).toEqual(true);
    });

    test('Draws input correctly', () => {
        const wrapper = shallow(<Form.Input type="text" value="test"></Form.Input>);

        expect(wrapper.matchesElement(<input className="form-control" type="text" value="test"/>)).toEqual(true);
    });

    test('Draws input correctly', () => {
        const wrapper = shallow(<Form.Input type="text" value="test"></Form.Input>);

        expect(wrapper.matchesElement(<input className="form-control" type="text" value="test"/>)).toEqual(true);
    });
});

describe('Alert danger widget tests', () => {
        test('No alerts initially', () => {
          const wrapper = shallow(<Alert />);
      
          expect(wrapper.matchesElement(<></>)).toEqual(true);
        });
        test('Open alert messsages and close the alert message', (done) => {
            const wrapper = shallow(<Alert />);
        
            Alert.danger('test');
        
            // Wait for events to complete
            setTimeout(() => {
              expect(
                wrapper.matchesElement(
                  <>
                    <div>
                      test<button>&times;</button>
                    </div>
                  </>
                )
              ).toEqual(true);
        
              done();
            });
          });

    test('Close alert message', (done) => {
        const wrapper = shallow(<Alert />);

        Alert.danger('test1');
        Alert.danger('test2');
        Alert.danger('test3');

        // Wait for events to complete
        setTimeout(() => {
        expect(
            wrapper.matchesElement(
            <>
            <div>
                test1<button>&times;</button>
            </div>
            <div>
                test2<button>&times;</button>
            </div>
            <div>
                test3<button>&times;</button>
            </div>
            </>
             )
            ).toEqual(true);

            wrapper.find('button.close').at(1).simulate('click');

            expect(
                wrapper.matchesElement(
                <>
                    <div>
                    test1<button>&times;</button>
                    </div>
                    <div>
                    test3<button>&times;</button>
                    </div>
                </>
                )
            ).toEqual(true);

            done();
            });
        });

  test('Close alert message', (done) => {
    const wrapper = shallow(<Alert />);

    Alert.danger('test1');
    Alert.danger('test2');
    Alert.danger('test3');

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.matchesElement(
          <>
            <div>
              test1<button>&times;</button>
            </div>
            <div>
              test2<button>&times;</button>
            </div>
            <div>
              test3<button>&times;</button>
            </div>
          </>
        )
      ).toEqual(true);

      wrapper.find('button.close').at(1).simulate('click');

      expect(
        wrapper.matchesElement(
          <>
            <div>
              test1<button>&times;</button>
            </div>
            <div>
              test3<button>&times;</button>
            </div>
          </>
        )
      ).toEqual(true);

      done();
    });
  });
});
