// @flow

import * as React from 'react';
import { Meny, Hjem } from '../src/index';
import { Historie, Sport, Naturogvitenskap, Samfunnogdagligliv, QuizNy, QuizLaget, QuizGjort } from '../src/quiz-components';
import { QuizDetaljer } from '../src/quiz-spill';
import { QuizSpm, QuizRediger, QuizSlettet } from '../src/quiz-rediger';
import { type Quiz } from '../src/quiz-service';
import { shallow } from 'enzyme';
import { Alert, Card, Row, Column, Form, Button, NavBar } from '../src/widgets';
import { NavLink } from 'react-router-dom';

jest.mock('../src/quiz-service', () => {
    class quizService {
      get() {
        return Promise.resolve([{ id: 1, tittel: 'Test quiz', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' }]);
      }
  
      getAll() {
        return Promise.resolve([
          { id: 1, tittel: 'test1', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
          { id: 2, tittel: 'test2', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
          { id: 3, tittel: 'test3', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
        ]);
      }
    }
    return new quizService();
  });

  describe('Meny tests', () => {
    test('Meny draws correctly', (done) => {
      const wrapper = shallow(<Meny />);
  
      // Wait for events to complete
      setTimeout(() => {
        expect(
          wrapper.containsAllMatchingElements([
            <NavBar brand="Quiz">
            <NavBar.Link to="/historie">Historie</NavBar.Link>
            <NavBar.Link to="/sport">Sport</NavBar.Link>
            <NavBar.Link to="/naturogvitenskap">Natur og vitenskap</NavBar.Link>
            <NavBar.Link to="/samfunnogdagligliv">Samfunn og dagligliv</NavBar.Link>
          </NavBar>
          ])
        ).toEqual(true);
        done();
      });
    });
  })

  describe('Hjem tests', () => {
    test('Hjem draws correctly', (done) => {
      const wrapper = shallow(<Hjem />);
  
      // Wait for events to complete
      setTimeout(() => {
        expect(
          wrapper.containsAllMatchingElements([
              <option key={1} value="test1"></option>,
              <option key={2} value="test2"></option>,
              <option key={3} value="test3"></option>,
          ])
        ).toEqual(true);
        done();
      });
    });

    test('Hjem draws Form.Input correctly', (done) => {
        const wrapper = shallow(<Hjem />);
    
     // $FlowExpectedError: do not type check next line.
        expect(wrapper.containsMatchingElement(<Form.Input value="" />));

        wrapper.find(Form.Input).simulate('change', { currentTarget: { value: 'test1' } });
        
        setTimeout(() => {
            // $FlowExpectedError: do not type check next line.
            expect(wrapper.containsMatchingElement(<Form.Input value="test1" />));
            done();
            });
     
      });

    test('Hjem correctly sets location on search', (done) => {
        const wrapper = shallow(<Hjem />);
        
        let sok = 'test1'
        // wrapper.find('#option').at(1).prop('test1');

        // wrapper.find(Button.Light).simulate('click');

        wrapper.find('datalist').simulate('change',{target: { value : 'test1'}});
        // Wait for events to complete
        setTimeout(() => {
        //   expect(location.hash).toEqual('#/quiz/test1/spm');
          expect(wrapper.find('#test1').props().value).toBe("test1");
          done();
        });
      });
      
      test('Finn quiz button click"', (done) => {
        const wrapper = shallow(<Hjem />);

        setTimeout(() => {
            wrapper.find(Button.Light).simulate('click');
          done();
        });
      });
  })