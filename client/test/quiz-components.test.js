// @flow

import * as React from 'react';
import { QuizNy, QuizLaget, QuizGjort, Sport, Historie, Naturogvitenskap, Samfunnogdagligliv } from '../src/quiz-components';
import { type Quiz } from '../src/quiz-service';
import { shallow } from 'enzyme';
import { Alert, Card, Row, Column, Form, Button, NavBar } from '../src/widgets';
import { NavLink } from 'react-router-dom';

jest.mock('../src/quiz-service', () => {
  class quizService {
    get() {
      return Promise.resolve([{ id: 1, tittel: 'Test quiz', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' }]);
    }

    getAll() {
      return Promise.resolve([
        { id: 1, tittel: 'test1', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
        { id: 2, tittel: 'test2', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
        { id: 3, tittel: 'test3', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
      ]);
    }
    
    getQuiz() {
      return Promise.resolve([       
        { id: 1, tittel: 'test1', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
        { id: 2, tittel: 'test1', kategori: 'Historie', spm: 'hva kan', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
        { id: 3, tittel: 'test1', kategori: 'Historie', spm: 'hva skal', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
      ])
    }

    getHistorie() {
      return Promise.resolve([
        { id: 1, tittel: 'test1', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
        { id: 2, tittel: 'test2', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
        { id: 3, tittel: 'test3', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
      ]);
    }
    getSport() {
      return Promise.resolve([
        { id: 1, tittel: 'test1', kategori: 'Sport', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
        { id: 2, tittel: 'test2', kategori: 'Sport', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
        { id: 3, tittel: 'test3', kategori: 'Sport', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
      ]);
    }
    getNatur() {
      return Promise.resolve([
        { id: 1, tittel: 'test1', kategori: 'Natur', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
        { id: 2, tittel: 'test2', kategori: 'Natur', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
        { id: 3, tittel: 'test3', kategori: 'Natur', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
      ]);
    }
    getSamfunn() {
      return Promise.resolve([
        { id: 1, tittel: 'test1', kategori: 'Samfunn', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
        { id: 2, tittel: 'test2', kategori: 'Samfunn', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
        { id: 3, tittel: 'test3', kategori: 'Samfunn', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
      ]);
    }

    update(
      id: number,
      tittel: string,
      kategori: string,
      spm: string,
      a: string,
      b: string,
      c: string,
      riktig_svar: string) {
      return Promise.resolve([{ id: 1, tittel: 'Test quiz', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' }]);
    }

    create(tittel: string, kategori: string, spm: string, a: string, b: string, c: string, riktig_svar: string) {
      return Promise.resolve(4); // Same as: return new Promise((resolve) => resolve(4));
    }

    delete(id: number) {
      return Promise.resolve();
    }
  }
  return new quizService();
});

//Historie
describe('Quiz component tests', () => {
  test('Historie draws correctly', (done) => {
    const wrapper = shallow(<Historie />);

    // Wait for events to complete
    setTimeout(() => {
      expect(
        wrapper.containsAllMatchingElements([
          <NavLink to="/quiz/test1/spm">test1</NavLink>,
          <NavLink to="/quiz/test2/spm">test2</NavLink>,
          <NavLink to="/quiz/test3/spm">test3</NavLink>,
        ])
      ).toEqual(true);
      done();
    });
  });

  test('Historie draws Form.Input correctly', (done) => {
    const wrapper = shallow(<Historie />);

 // $FlowExpectedError: do not type check next line.
    expect(wrapper.containsMatchingElement(<Form.Input value="" />));

    wrapper.find(Form.Input).simulate('change', { currentTarget: { value: 'test1' } });
    
    setTimeout(() => {
        // $FlowExpectedError: do not type check next line.
        expect(wrapper.containsMatchingElement(<Form.Input value="test1" />));
        done();
        });
  });

  test('Finn quiz button click"', (done) => {
    const wrapper = shallow(<Historie />);

    setTimeout(() => {
        wrapper.find(Button.Light).simulate('click');
      done();
    });
  });

  test('Historie correctly sets location on new', (done) => {
    const wrapper = shallow(<Historie />);

    wrapper.find(Button.Success).simulate('click');
    // Wait for events to complete
    setTimeout(() => {
      expect(location.hash).toEqual('#/quizny');
      done();
    });
  });
});
  //Sport
  describe('Quiz component tests', () => {
    test('Sport draws correctly', (done) => {
      const wrapper = shallow(<Sport />);

      // Wait for events to complete
      setTimeout(() => {
        expect(
          wrapper.containsAllMatchingElements([
            <NavLink to="/quiz/test1/spm">test1</NavLink>,
            <NavLink to="/quiz/test2/spm">test2</NavLink>,
            <NavLink to="/quiz/test3/spm">test3</NavLink>,
          ])
        ).toEqual(true);
        done();
      });
    });

    test('Sport draws Form.Input correctly', (done) => {
      const wrapper = shallow(<Sport />);
  
   // $FlowExpectedError: do not type check next line.
      expect(wrapper.containsMatchingElement(<Form.Input value="" />));
  
      wrapper.find(Form.Input).simulate('change', { currentTarget: { value: 'test1' } });
      
      setTimeout(() => {
          // $FlowExpectedError: do not type check next line.
          expect(wrapper.containsMatchingElement(<Form.Input value="test1" />));
          done();
          });
    });
  
    test('Finn quiz button click"', (done) => {
      const wrapper = shallow(<Sport />);
  
      setTimeout(() => {
          wrapper.find(Button.Light).simulate('click');
        done();
      });
    });

    test('Sport correctly sets location on new', (done) => {
      const wrapper = shallow(<Sport />);

      wrapper.find(Button.Success).simulate('click');
      // Wait for events to complete
      setTimeout(() => {
        expect(location.hash).toEqual('#/quizny');
        done();
      });
    });
  });
    //Natur og vitenskap
    describe('Quiz component tests', () => {
      test('Naturogvitenskap draws correctly', (done) => {
        const wrapper = shallow(<Naturogvitenskap />);

        // Wait for events to complete
        setTimeout(() => {
          expect(
            wrapper.containsAllMatchingElements([
              <NavLink to="/quiz/test1/spm">test1</NavLink>,
              <NavLink to="/quiz/test2/spm">test2</NavLink>,
              <NavLink to="/quiz/test3/spm">test3</NavLink>,
            ])
          ).toEqual(true);
          done();
        });
      });

      test('Historie draws Form.Input correctly', (done) => {
        const wrapper = shallow(<Naturogvitenskap />);
    
     // $FlowExpectedError: do not type check next line.
        expect(wrapper.containsMatchingElement(<Form.Input value="" />));
    
        wrapper.find(Form.Input).simulate('change', { currentTarget: { value: 'test1' } });
        
        setTimeout(() => {
            // $FlowExpectedError: do not type check next line.
            expect(wrapper.containsMatchingElement(<Form.Input value="test1" />));
            done();
            });
      });
    
      test('Finn quiz button click"', (done) => {
        const wrapper = shallow(<Naturogvitenskap />);
    
        setTimeout(() => {
            wrapper.find(Button.Light).simulate('click');
          done();
        });
      });

      test('Naturogvitenskap correctly sets location on new', (done) => {
        const wrapper = shallow(<Naturogvitenskap />);

        wrapper.find(Button.Success).simulate('click');
        // Wait for events to complete
        setTimeout(() => {
          expect(location.hash).toEqual('#/quizny');
          done();
        });
      });
    });
      //Samfunn og dagligliv
      describe('Quiz component tests', () => {
        test('Samfunnogdagligliv draws correctly', (done) => {
          const wrapper = shallow(<Samfunnogdagligliv />);

          // Wait for events to complete
          setTimeout(() => {
            expect(
              wrapper.containsAllMatchingElements([
                <option key={1} value="test1"></option>,
                <option key={2} value="test2"></option>,
                <option key={3} value="test3"></option>,
                <NavLink to="/quiz/test1/spm">test1</NavLink>,
                <NavLink to="/quiz/test2/spm">test2</NavLink>,
                <NavLink to="/quiz/test3/spm">test3</NavLink>,
              ])
            ).toEqual(true);
            done();
          });
        });

        test('Historie draws Form.Input correctly', (done) => {
          const wrapper = shallow(<Samfunnogdagligliv />);
      
       // $FlowExpectedError: do not type check next line.
          expect(wrapper.containsMatchingElement(<Form.Input value="" />));
      
          wrapper.find(Form.Input).simulate('change', { currentTarget: { value: 'test1' } });
          
          setTimeout(() => {
              // $FlowExpectedError: do not type check next line.
              expect(wrapper.containsMatchingElement(<Form.Input value="test1" />));
              done();
              });
        });
      
        test('Finn quiz button click"', (done) => {
          const wrapper = shallow(<Samfunnogdagligliv />);
      
          setTimeout(() => {
              wrapper.find(Button.Light).simulate('click');
            done();
          });
        });

        test('Samfunnogdagligliv correctly sets location on new', (done) => {
          const wrapper = shallow(<Samfunnogdagligliv />);

          wrapper.find(Button.Success).simulate('click');
          // Wait for events to complete
          setTimeout(() => {
            expect(location.hash).toEqual('#/quizny');
            done();
          });
        });
      });

      describe('Quiz ny tests', () => {
        test('QuizNy correctly sets location on create', (done) => {
          const wrapper = shallow(<QuizNy />);

          wrapper.find('#tittel').simulate('change', { currentTarget: { value: 'ny quiz' } });
          wrapper.find('#kategori').simulate('change', { currentTarget: { value: 'Historie' } });
          wrapper.find('#spm').simulate('change', { currentTarget: { value: 'hva' } });
          wrapper.find('#a').simulate('change', { currentTarget: { value: 'valga' } });
          wrapper.find('#b').simulate('change', { currentTarget: { value: 'valgb' } });
          wrapper.find('#c').simulate('change', { currentTarget: { value: 'valgc' } });
          wrapper.find('#riktig_svar').simulate('change', { currentTarget: { value: 'a' } });
          // $FlowExpectedError
          expect(wrapper.containsMatchingElement(<Form.Input value="ny quiz" />)).toEqual(true);

          wrapper.find(Button.Success).simulate('click');
          // Wait for events to complete
          setTimeout(() => {
            expect(location.hash).toEqual('#/quizny/laget');
            done();
          });
        });

        test('QuizNy draws correctly', (done) => {
          const wrapper = shallow(<QuizNy tittel="Hei" />);

          wrapper.find('#tittel').simulate('change', { currentTarget: { value: 'ny quiz' } });
          wrapper.find('#kategori').simulate('change', { currentTarget: { value: 'Historie' } });
          wrapper.find('#spm').simulate('change', { currentTarget: { value: 'hva' } });
          wrapper.find('#a').simulate('change', { currentTarget: { value: 'valga' } });
          wrapper.find('#b').simulate('change', { currentTarget: { value: 'valgb' } });
          wrapper.find('#c').simulate('change', { currentTarget: { value: 'valgc' } });
          wrapper.find('#riktig_svar').simulate('change', { currentTarget: { value: 'a' } });
          
          
          wrapper.find(Button.Success).simulate('click');

          // $FlowExpectedError
          setTimeout(() => {
            expect(wrapper.containsMatchingElement(<Form.Input value="ny quiz" />)).toEqual(true);
            done();
          });
        });
       });

      describe('QuizLaget tests', () => {
        test('QuizLaget draws correctly', (done) => {
          const wrapper = shallow(<QuizLaget />);

          // Wait for events to complete
          setTimeout(() => {
            expect(
              wrapper.containsMatchingElement(<Card title="Gratulerer! Du har opprettet en ny quiz"></Card>)).toEqual(true);
              done();
          });
        });
      })
