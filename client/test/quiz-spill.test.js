 // @flow

import * as React from 'react';
import { QuizDetaljer } from '../src/quiz-spill';
import { type Quiz } from '../src/quiz-service';
import { shallow } from 'enzyme';
import { Alert, Card, Row, Column, Form, Button, NavBar } from '../src/widgets';
import { NavLink } from 'react-router-dom';

jest.mock('../src/quiz-service', () => {
    class quizService {     
      getQuiz() {
        return Promise.resolve([       
          { id: 1, tittel: 'test1', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
          { id: 2, tittel: 'test1', kategori: 'Historie', spm: 'hva kan', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
          { id: 3, tittel: 'test1', kategori: 'Historie', spm: 'hva skal', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
          { id: 3, tittel: 'test2', kategori: 'Historie', spm: 'hva skal', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
        ])
      }
    }
    return new quizService();
  });

  describe('QuizDetaljer tests', () => {
    test('QuizDetaljer sets location on rediger button', (done) => {
      const wrapper = shallow(<QuizDetaljer match={{ params: { tittel: 'test1' } }}/>);
        
      setTimeout(() => {
        wrapper.find('#quizrediger').simulate('click');
        expect(location.hash).toEqual('#/rediger/test1/alleSpm');
        done();
          done();
      });
    });
  
    test('QuizDetaljer draws correctly radio[0]', (done) => {
        const wrapper = shallow(<QuizDetaljer match={{ params: { tittel: 'test1' } }}/>);

        setTimeout(() => {
          expect(
            wrapper.containsAllMatchingElements([
             <input type="radio" id="valg1" name="alternativ" value="a"></input>,
             <label htmlFor="valg1">a: svar1</label>,
             <input type="radio" id="valg2" name="alternativ" value="b"></input>,
             <label htmlFor="valg2">b: svar2</label>,
             <input type="radio" id="valg3" name="alternativ" value="c"></input>,
             <label htmlFor="valg3">c: svar3</label>,
            ])
          ).toEqual(true);
          done();
        });
      });

      test('QuizDetaljer clicks "Prøv igjen" and "Detaljer"', (done) => {
        const wrapper = shallow(<QuizDetaljer match={{ params: { tittel: 'test2' } }}/>);

        setTimeout(() => {
            wrapper.find('#valg1').simulate('change');
            wrapper.find('#sendsvar').simulate('click'); 
          done();
        });
      });
  
})