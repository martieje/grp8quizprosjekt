// @flow

import * as React from 'react';
import { QuizSpm, QuizRediger, QuizSlettet } from '../src/quiz-rediger';
import { type Quiz } from '../src/quiz-service';
import { shallow } from 'enzyme';
import { Alert, Card, Row, Column, Form, Button, NavBar } from '../src/widgets';
import { NavLink } from 'react-router-dom';

jest.mock('../src/quiz-service', () => {
    class quizService {
      get() {
        return Promise.resolve([{ id: 1, tittel: 'test1', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' }]);
      }
      
      getQuiz() {
        return Promise.resolve([       
          { id: 1, tittel: 'test1', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
          { id: 2, tittel: 'test1', kategori: 'Historie', spm: 'hva kan', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
          { id: 3, tittel: 'test1', kategori: 'Historie', spm: 'hva skal', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' },
        ])
      }
  
      update(
        id: number,
        tittel: string,
        kategori: string,
        spm: string,
        a: string,
        b: string,
        c: string,
        riktig_svar: string) {
        return Promise.resolve([{ id: 1, tittel: 'test1', kategori: 'Historie', spm: 'hva er', a: 'svar1', b: 'svar2', c: 'svar3', riktig_svar: 'a' }]);
      }
  
      delete(id: number) {
        return Promise.resolve();
      }

      deleteQuiz(tittel:string) {
          return Promise.resolve();
      }
    }
    return new quizService();
  });

  describe('QuizSpm tests', () => {
    test('QuizSpm draws correctly', (done) => {
      const wrapper = shallow(<QuizSpm match={{ params: { tittel: 'test1' } }}/>);

      // Wait for events to complete
      setTimeout(() => {
        expect(
          wrapper.containsAllMatchingElements([
          <NavLink to="/rediger/1/spm">hva er</NavLink>,
          <NavLink to="/rediger/2/spm">hva kan</NavLink>,
          <NavLink to="/rediger/3/spm">hva skal</NavLink>,
          ])
        ).toEqual(true);
          done();
      });
    });

    test('QuizSpm sets location on delete', (done) => {
        const wrapper = shallow(<QuizSpm match={{ params: { tittel: 'test1' } }}/>);
  
        wrapper.find(Button.Danger).simulate('click');

        setTimeout(() => {
          expect(location.hash).toEqual('#/slettet');
          done();
        });
      });

      test('QuizSpm sets location on cancel', (done) => {
        const wrapper = shallow(<QuizSpm match={{ params: { tittel: 'test1' } }}/>);
  
        wrapper.find(Button.Light).simulate('click');

        setTimeout(() => {
          expect(location.hash).toEqual('#/quiz/test1/spm');
          done();
        });
      });
    
  })
  
  describe('QuizRediger component tests', () => {
    test('QuizRediger correctly sets location on rediger', (done) => {
        const wrapper = shallow(<QuizRediger match={{ params: { id: 1 } }} />);
        
        wrapper.find('#spm').simulate('change', { currentTarget: { value: 'hva' } });
        wrapper.find('#a').simulate('change', { currentTarget: { value: 'valga' } });
        wrapper.find('#b').simulate('change', { currentTarget: { value: 'valgb' } });
        wrapper.find('#c').simulate('change', { currentTarget: { value: 'valgc' } });
        wrapper.find('#riktig_svar').simulate('change', { currentTarget: { value: 'a' } });
        // $FlowExpectedError
        expect(wrapper.containsMatchingElement(<Form.Input id="spm" value="hva" />)).toEqual(true);

        wrapper.find(Button.Success).simulate('click');
        // Wait for events to complete
        setTimeout(() => {
          expect(location.hash).toEqual('#/rediger/undefined/alleSpm'); //fikk error, klarte ikke å hente tittel til quizen
          done();
        });
      });

      test('QuizRediger delete correctly sets location on delete', (done) => {
        const wrapper = shallow(<QuizRediger match={{ params: { id: 1 } }} />);

        wrapper.find(Button.Danger).simulate('click');

        setTimeout(() => {
          expect(location.hash).toEqual('#/rediger/undefined/alleSpm'); //samme som ovenfor
          done();
        });
      });
  })

  describe('QuizSlettet tests', () => {
    test('QuizSlettet draws correctly', (done) => {
      const wrapper = shallow(<QuizSlettet />);

      // Wait for events to complete
      setTimeout(() => {
        expect(
          wrapper.containsMatchingElement(<Card title="Du har nå slettet en quiz! Velg kategori for flere quizer"></Card>)).toEqual(true);
          done();
      });
    });
  })