-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: mysql.stud.ntnu.no
-- Generation Time: Nov 20, 2020 at 05:21 AM
-- Server version: 5.7.31-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-0ubuntu0.16.04.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `miagi_grp8_prosjekt_db1`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_quiz`
--

CREATE TABLE `tbl_quiz` (
  `id` int(11) NOT NULL,
  `kategori` text NOT NULL,
  `tittel` text NOT NULL,
  `spm` text,
  `a` text,
  `b` text,
  `c` text,
  `riktig_svar` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_quiz`
--

INSERT INTO `tbl_quiz` (`id`, `kategori`, `tittel`, `spm`, `a`, `b`, `c`, `riktig_svar`) VALUES
(51, 'Historie', 'Historie blandet', 'I 1924 ble de første olympiske vinterleker arrangert. I hvilket sted og land ble arrangementet avholdt?', 'Chamonix, Frankrike', 'Spania, Madrid', 'Sveits, Bern', 'a'),
(52, 'Historie', 'Historie blandet', 'Hvilket norsk politisk parti ble stiftet 1887 i Arendal?', 'Arbeiderpartiet', 'MDG', 'Fremskrittspartiet', 'a'),
(53, 'Historie', 'Historie blandet', 'Hvilken væpnet konflikt, med stort antall falne på hver side, foregikk i årene 1861 til 1865?', 'Den amerikanske borgerkrigen', 'Andre verdenskrig', 'Vietnamkrigen', 'a'),
(54, 'Historie', 'Historie blandet', 'Hvem ble kronet fransk keiser i 1804?', 'Napoleon Bonaparte', 'Keiser Kusko', 'Queen Victoria', 'a'),
(55, 'Historie', 'Revolusjoner', 'Hva var slagordet under Den franske revolusjon?', 'Frihet, likhet og brorskap', 'Ulikhet og frihet', 'Frivillighet og brorskap', 'a'),
(56, 'Historie', 'Revolusjoner', 'Hva kaller vi revolusjonen som tok til på 1700-tallet med oppfinnelser som blant annet "Spinning Jenny"?', 'Den industrielle revolusjon', 'Den tekniske revolusjon', 'Den kulturelle revolusjon', 'a'),
(57, 'Historie', 'Revolusjoner', 'Hvem grunnla bolsjevikpartiet som tok makten i Russland etter sistnevnte revolusjon?', 'Putin', 'Stalin', 'Vladimir Lenin', 'c'),
(58, 'Historie', 'Revolusjoner', 'I hvilket land fant Kulturrevolusjonen sted?', 'Russland', 'Kina', 'USA', 'b'),
(59, 'Sport', 'Mesterskap', 'Hvor mange individuelle OL-gullmedaljer vant Oddvar brå i løpet av sin aktive karrière?', 'Ingen', '1', '3', 'a'),
(60, 'Sport', 'Mesterskap', 'Hvor ble vinter-OL arrangert i 2010?', 'Frankrike, Lyon', 'Vancouver, Canada', 'England, London', 'b'),
(61, 'Sport', 'Mesterskap', 'I hvilket år ble første Fotball VM spilt?', '1946', '1905', '1930', 'c'),
(62, 'Sport', 'Mesterskap', 'Hva er det vanlig å legge til på landslagets logo etter at man har vunnet et Fotball VM?', 'En stjerne', 'En firkant', 'En sirkel', 'a'),
(63, 'Sport', 'Sport blandet', 'Hvilken nasjon har før Fotball VM 2018 vunnet flest ganger?', 'Brasil, 5 ganger', 'Spania, 5 ganger', 'Frankrike, 5 ganger', 'a'),
(64, 'Sport', 'Sport blandet', 'Hva heter det plagsomme blåse-"instrumentet" som fikk sitt gjennombrudd i VM i Sør Afrika 2010?', 'Trompet', 'Tromme', 'Vuvuzela', 'c'),
(65, 'Sport', 'Sport blandet', 'Når var forrige gang Norges landslag spilte i et VM-sluttspill?', '1961', '1998', '2000', 'b'),
(66, 'Sport', 'Sport blandet', 'Hvilken idrettsutøver var den første til å vinne fem Wimbledon titler på rad?', 'Bjørn Borg', 'Rafael Nadal', 'Novak Dokovic', 'a'),
(67, 'Natur', 'Grønt', 'Hvilken farge må du blande blått med for å få grønt?', 'Rødt', 'Gult', 'Brunt', 'b'),
(68, 'Natur', 'Grønt', 'Hva heter tidssonen som brukes i blant annet England?', 'Greenwich Mean time', 'Amazon time', 'Casey Time', 'a'),
(69, 'Natur', 'Grønt', 'Hvilken medisinsk tilstand lider du av dersom du har “glaukom”?', 'Hurler syndrom', 'Epilepsi', 'Grønn stær', 'c'),
(70, 'Natur', 'Grønt', 'Omlag 2% av verdens befolkning har grønne øyne. Hva er den vanligste øyefargen?', 'Brun', 'Blå', 'Grå', 'a'),
(71, 'Natur', 'Pandemier', 'Hvilken pandemi antar historikere at kom til Norge  i år 1347?', 'Spanskesyken', 'Svartedauden', 'Svineinfluensa', 'b'),
(72, 'Natur', 'Pandemier', 'Hvilken norsk by brøt denne pesten ut i først?', 'Trondheim', 'Bergen', 'Tromsø', 'b'),
(73, 'Natur', 'Pandemier', 'Gulfeber hadde et stort utbrudd på 1800-tallet. På hvilken måte smitter viruset som forårsaker denne sykdommen?', 'Ved dråpesmitte', 'Ved blodsmitte', 'Ved myggstikk', 'c'),
(74, 'Natur', 'Pandemier', 'Hva heter den kinesiske millionbyen hvor en ny type coronavirus ble påvist i desember 2019?', 'Wuhan', 'Tianjin', 'Shenyang', 'a'),
(75, 'Samfunn', 'Halloween', 'Hva er det norske ordet for Halloween?', 'Allemannsaften', 'Allehelgensaften', 'Ingen, oversettes ikke', 'b'),
(76, 'Samfunn', 'Halloween', 'Hva er det engelske motstykket til ordene “knask eller knep”?', 'Crunch or trick', '  Trick or treat', '  Candy or war', 'b'),
(77, 'Samfunn', 'Halloween', 'Hva heter stedet hvor slottet til grev Dracula lå?', 'Transilvania', 'Temecula', 'Titusville', 'a'),
(78, 'Samfunn', 'Halloween', 'På hvilken dato feires Halloween?', '30. oktober', '31. oktober', '1. november', 'b'),
(79, 'Samfunn', 'Nobels fredspris', 'Hvem ble tildelt Nobels fredspris for 2020?', 'Reportere uten grenser', '  International Crisis Group', '  Verdens matvareprogram (WFP)', 'c'),
(80, 'Samfunn', 'Nobels fredspris', 'Foruten Knut Hamsun og Sigrid Undset, hvilke andre nordmenn har vunnet Nobelprisen i litteratur?', '  Bjørnstjerne Bjørnson', '  Ludvig Holberg', '  Alexander Kielland', 'a'),
(81, 'Samfunn', 'Nobels fredspris', '  Hvor mange nobelpriser deles ut hvert år, slik det sto beskrevet i Alfred Nobels testamente fra 1895?', '7', '5', '3', 'b'),
(82, 'Samfunn', 'Nobels fredspris', 'Hva er det maksimale antallet personer (hvis det ikke er en organisasjon) som kan dele en nobelpris?', '3', '2', '4', 'a');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_quiz`
--
ALTER TABLE `tbl_quiz`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_quiz`
--
ALTER TABLE `tbl_quiz`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
