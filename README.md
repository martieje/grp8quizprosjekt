# grp8QuizProsjekt

Prosjektet vårt er en quiz-app hvor en kan gjøre, redigere, lage og slette quizer. Etter gjennomførring av quizer får man en poengscore.
For å kunne spille må man laste ned og installere noen filer:

1. For å laste ned quiz-appen må man benytte seg av git clone:
   Skriv i terminal eller i preferert kodemiljø: git clone https://gitlab.stud.idi.ntnu.no/martieje/grp8quizprosjekt.git

2. Opprett config.js filer i server/src/config.js og server/test/config.js. Eksempel:

```js
// @flow

process.env.MYSQL_HOST = "mysql.stud.ntnu.no";
process.env.MYSQL_USER = "username_quiz";
process.env.MYSQL_PASSWORD = "username_quiz";
process.env.MYSQL_DATABASE = "username_quiz_dev";
```

`server/test/config.js`:

```js
// @flow

process.env.MYSQL_HOST = "mysql.stud.ntnu.no";
process.env.MYSQL_USER = "username_quiz";
process.env.MYSQL_PASSWORD = "username_quiz";
process.env.MYSQL_DATABASE = "username_quiz_test";
```

3. En template fil for database oppsettet finner du på stien: '/grp8QuizProsjekt/tbl_quiz.sql'. Opprett en database for både "username_quiz_dev" og "username_quiz_test".

4. Du er nødt til å installere nødvendige filer

Server siden:

```sh
cd server
npm install
```

Klient siden:

```sh
cd client
npm install
```

5. I en egen terminal, gå til /client og skriv: 'npm start' for å pakke sammen klient filene.

6. I en annen terminal, gå til /server og skriv: 'npm start' for å starte serveren

7. Skriv 'localhost:3001' i URL feltet i en nettleser. Trykk enter.

8. Nå er det bare å begynne å spille i quiz-appen.

Lykke til!

ps. For å kjøre testing skriver man 'npm test' for klient og server siden.
