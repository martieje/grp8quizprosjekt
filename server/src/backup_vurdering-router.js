// @flow

//Begynte på en vurderingsfunksjonalitet, men rakk ikke ferdigstille

import express from 'express';
import vurderingService, { type Vurdering } from './vurdering-service';

/**
 * Express router containing quiz methods.
 */
const router: express$Router<> = express.Router();

router.get(`/quiz/:id/gjennomført`, (request, response) => {
    const id = Number(request.params.id);
    const vurdering = Number(request.params.vurdering)
    vurderingService
        .get(id)
        .then((vurdering) => (vurdering ? response.send(vurdering) : response.status(404).send('Vurdering ikke funnet')))
        .catch((error: Error) => response.status(500).send(error));
});

router.post(`/quiz/:id/gjennomført`, (request, response) => {
  const data = request.body;
  if (
    data &&
    typeof data.vurdering == 'number' && 
    typeof data.quizId == 'number'
  )

    vurderingService
      .create(data.vurdering, data.quizId)
      .then((id) => response.send({ id: id }))
      .catch((error: Error) => response.status(500).send(error));
  else response.status(400).send('Mangler vurdering');
});

export default router;
