// @flow

//Begynte på en innloggingsfunksjonalitet, men rakk ikke ferdigstille

import pool from './mysql-pool';

export type Bruker = {
  username: string,
  password: string
};

class LoginService {
  
  create(username: string, password: string) {
    return new Promise<string>((resolve, reject) => {
      pool.query('INSERT INTO users SET username=?, password=?', [username, password], (error, results) => {
        if (error) return reject(error);
        if (!results.insertId) return reject(new Error('No row inserted'));

        resolve(String(results.insertId));
      });
    });
  }

}

const loginService = new LoginService();
export default loginService;