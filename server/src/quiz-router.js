// @flow
import express from 'express';
import quizService, { type Quiz } from './quiz-service';

/**
 * Express router containing quiz methods.
 */
const router: express$Router<> = express.Router();

////////////////////////////////////////////////////////

router.get('/historie', (request, response) => {
  quizService
    .getHistorie()
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});
router.get('/sport', (request, response) => {
  quizService
    .getSport()
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});
router.get('/naturogvitenskap', (request, response) => {
  quizService
    .getNatur()
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});
router.get('/samfunnogdagligliv', (request, response) => {
  quizService
    .getSamfunn()
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});
////////////////////////////////////////////////////////

router.get('/rediger/:id/spm', (request, response) => {
  const id = Number(request.params.id);
  quizService
    .get(id)
    .then((quiz) => (quiz ? response.send(quiz) : response.status(404).send('Quiz not found')))
    .catch((error: Error) => response.status(500).send(error));
});

router.get('/rediger/:tittel/alleSpm', (request, response) => {
  const tittel = request.params.tittel;
  quizService
    .getQuiz(tittel)
    .then((quiz) => (quiz ? response.send(quiz) : response.status(404).send('Quiz not found')))
    .catch((error: Error) => response.status(500).send(error));
 });

router.get('/quiz/:tittel/spm', (request, response) => {
  const tittel = request.params.tittel;
  quizService
    .getQuiz(tittel)
    .then((rows) =>  response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});

router.get('/', (request, response) => {
  quizService
    .getAll()
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});

////////////////////////////////////////////////////////

router.post('/quizny', (request, response) => {
  const data = request.body;
  if (
    data &&
    typeof data.tittel == 'string' &&
    data.tittel.length != 0 &&
    typeof data.spm == 'string' &&
    data.spm.length != 0 &&
    typeof data.a == 'string' &&
    data.a.length != 0 &&
    typeof data.b == 'string' &&
    data.b.length != 0 &&
    typeof data.c == 'string' &&
    data.c.length != 0 &&
    typeof data.riktig_svar == 'string' &&
    data.riktig_svar.length != 0 &&
    typeof data.kategori == 'string' &&
    data.kategori.length != 0
  )
    quizService
      .create(data.tittel, data.kategori, data.spm, data.a, data.b, data.c, data.riktig_svar)
      .then((id) => response.send({ id: id }))
      .catch((error: Error) => response.status(500).send(error));
  else response.status(400).send('Mangler tittel, spørsmål, valg eller riktig svar');
});

////////////////////////////////////////////////////////

router.delete('/rediger/:id/spm', (request, response) => {
  quizService
    .delete(Number(request.params.id))
    .then((result) => response.send())
    .catch((error: Error) => response.status(500).send(error));
});
router.delete('/rediger/:tittel/alleSpm', (request, response) => {
  quizService
    .deleteQuiz(String(request.params.tittel))
    .then((result) => response.send())
    .catch((error: Error) => response.status(500).send(error));
});

router.put('/quiz/:id', (request, response) => {
  const data = request.body;
  const id = Number(request.params.id);
  if ( 
    data &&
    typeof data.tittel == 'string' &&
    data.tittel.length != 0 &&
    typeof data.spm == 'string' &&
    data.spm.length != 0 &&
    typeof data.a == 'string' &&
    data.a.length != 0 &&
    typeof data.b == 'string' &&
    data.b.length != 0 &&
    typeof data.c == 'string' &&
    data.c.length != 0 &&
    typeof data.riktig_svar == 'string'
  )

    quizService
      .update(id, data.tittel, data.spm, data.a, data.b, data.c, data.riktig_svar)
      .then((result) => response.send())
      .catch((error: Error) => response.status(400).send(error));
});

export default router;
