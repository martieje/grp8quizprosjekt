// @flow

import pool from './mysql-pool';

export type Quiz = {
  id: number,
  tittel: string,
  kategori: string,
  spm: string,
  a: string,
  b: string,
  c: string,
  riktig_svar: string
};
class QuizService { 
  /**
   * Hent quiz
   */
  get(id: number) {
    return new Promise<?Quiz>((resolve, reject) => {
      pool.query('SELECT * FROM tbl_quiz WHERE id = ?', [id], (error, results: Quiz[]) => {
        if (error) return reject(error);

        resolve(results[0]);
      });
    });
  }
  getQuiz(tittel: string) {
    return new Promise<Quiz[]>((resolve, reject) => {
      pool.query('SELECT * FROM tbl_quiz WHERE tittel=?', [tittel], (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
     });
  } 

  ////////////////////////////////////////////////////
  /**
   * Get all quizzer.
   */
  getAll() {
    return new Promise<Quiz[]>((resolve, reject) => {
      pool.query('SELECT DISTINCT tittel From tbl_quiz', (error, results) => {
        if(error) return reject(error);

        resolve(results);
      });
    });
  }
  getHistorie() {
    return new Promise<Quiz[]>((resolve, reject) => {
      pool.query('SELECT DISTINCT tittel FROM tbl_quiz WHERE kategori = "Historie" ', (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }
  getSport() {
    return new Promise<Quiz[]>((resolve, reject) => {
      pool.query('SELECT DISTINCT tittel FROM tbl_quiz WHERE kategori = "Sport" ', (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }
  getNatur() {
    return new Promise<Quiz[]>((resolve, reject) => {
      pool.query('SELECT DISTINCT tittel FROM tbl_quiz WHERE kategori = "Natur" ', (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }
  getSamfunn() {
    return new Promise<Quiz[]>((resolve, reject) => {
      pool.query('SELECT DISTINCT tittel FROM tbl_quiz WHERE kategori = "Samfunn"', (error, results) => {
        if (error) return reject(error);

        resolve(results);
      });
    });
  }
//////////////////////////////////////////////////////
  /**
   * Opprett ny quiz med den gitte tittelen
   *
   * Resolve -> den nyopprettede quiz-id'en.
   */
  create(tittel: string, kategori: string, spm: string, a: string, b: string, c: string, riktig_svar: string) {
    return new Promise<number>((resolve, reject) => {
      pool.query('INSERT INTO tbl_quiz SET tittel=?, kategori=?, spm=?, a=?, b=?, c=?, riktig_svar=?', [tittel, kategori, spm, a, b, c, riktig_svar], (error, results) => {
        if (error) return reject(error);
        if (!results.insertId) return reject(new Error('No row inserted'));

        resolve(Number(results.insertId));
      });
    });
  }

  /**
   * Sletter spørsmål med gitt id, eller sletter hele quiz med gitt tittel
   */
  delete(id: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query('DELETE FROM tbl_quiz WHERE id = ?', [id], (error, results) => {
        if (error) return reject(error);
        if (!results.affectedRows) reject(new Error('No row deleted'));

        resolve();
      });
    });
  }
  deleteQuiz(tittel: string) {
    return new Promise<void>((resolve, reject) => {
      pool.query('DELETE FROM tbl_quiz WHERE tittel=?', [tittel], (error, results) => {
        if (error) return reject(error);
        if (!results.affectedRows) reject(new Error('No row deleted'));

        resolve();
      });
    });
  }

  /**
   * Redigerer spørsmål med gitt id
   */
  update(id: number, tittel: string, spm: string, a: string, b: string, c: string, riktig_svar: string) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE tbl_quiz SET tittel = ?, spm=?, a=?, b=?, c=?, riktig_svar=? WHERE id = ?',
        [tittel, spm, a, b, c, riktig_svar, id],
        (error, results) => {
          console.log(error);
          if (error) return reject(error);

          resolve();
        }
      );
    });
  }
}

const quizService = new QuizService();
export default quizService;
