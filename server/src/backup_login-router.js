// @flow

//Begynte på en innloggingsfunksjonalitet, men rakk ikke ferdigstille

import express from 'express';
import loginService, { type Bruker } from './login-service';
// const bcrypt = require('bcryptjs');

/**
 * Express router containing task methods.
 */
const router: express$Router<> = express.Router();

router.post('/login', (request, response) => {
    const data = request.body;
    if (
        data && 
        typeof data.username == 'string' && 
        data.username.length != 0 &&
        typeof data.password == 'string' &&
        data.password.length != 0
      )
      loginService
        .create(data.username, data.password)
        .then((id) => response.send({ id: id }))
        .catch((error: Error) => response.status(500).send(error));
    else response.status(400).send('Username or password cannot be empty');
  });

export default router;
