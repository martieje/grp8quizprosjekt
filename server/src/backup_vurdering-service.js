// @flow

//Begynte på en vurderingsfunksjonalitet, men rakk ikke ferdigstille

import pool from './mysql-pool';

export type Vurdering = {
  vurdering: number;
  quizId: number;
}
class VurderingService {
  /**
   * Get quiz with given id.
   */
  get(id: number) {
    return new Promise<?Vurdering>((resolve, reject) => {
      pool.query('SELECT * FROM Vurderinger WHERE id = ?', [id], (error, results: Vurdering[]) => {
        if (error) return reject(error);

        resolve(results[0]);
      });
    });
  }

  create(vurdering: number, quizId: number) {
    return new Promise<number>((resolve, reject) => {
       pool.query('INSERT INTO Vurderinger (vurdering, quizId) VALUES (?, ?)', [vurdering, quizId], (error, results) => {
        if (error) return reject(error);
        if (!results.insertId) return reject(new Error('No row inserted'));

        resolve(Number(results.insertId));
      });
    });
  }
}

const vurderingService = new VurderingService();
export default vurderingService;
