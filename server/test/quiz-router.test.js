//@flow

import axios from 'axios';
import pool from '../src/mysql-pool';
import app from '../src/app';
import quizService, { type Quiz } from '../src/quiz-service';

const testQuizs: Quiz[] = [
  { id: 1, 
    tittel: 'Testquiz1', 
    kategori: 'Historie', 
    spm: 'testspm', 
    a: 'aaa', 
    b: 'bbb', 
    c: 'ccc', 
    riktig_svar:'a'
  },
  { id: 2, 
    tittel: 'Testquiz2', 
    kategori: 'Sport',
    spm: 'testspm', 
    a: 'aaa', 
    b: 'bbb', 
    c: 'ccc', 
    riktig_svar:'b' 
  },
  { id: 3, 
    tittel: 'Testquiz3', 
    kategori: 'Samfunn', 
    spm: 'testspm', 
    a: 'aaa', 
    b: 'bbb', 
    c: 'ccc', 
    riktig_svar:'c' 
  },
  { id: 4, 
    tittel: 'Testquiz4', 
    kategori: 'Natur', 
    spm: 'testspm', 
    a: 'aaa', 
    b: 'bbb', 
    c: 'ccc', 
    riktig_svar:'a' 
  },
];

// Since API is not compatible with v1, API version is increased to v2
axios.defaults.baseURL = 'http://localhost:3002/api/v2';

let webServer;
beforeAll((done) => {
  // Use separate port for testing
  webServer = app.listen(3002, () => done());
});

beforeEach((done) => {
  // Delete all quizs, and reset id auto-increment start value
  pool.query('TRUNCATE TABLE tbl_quiz', (error) => {
    if (error) return done.fail(error);

    // Create testQuizs sequentially in order to set correct id, and call done() when finished
    quizService
      .create(
        testQuizs[0].tittel, 
        testQuizs[0].kategori, 
        testQuizs[0].spm, 
        testQuizs[0].a, 
        testQuizs[0].b, 
        testQuizs[0].c, 
        testQuizs[0].riktig_svar
      )
      .then(() => // Create quizTask[1] after quizTask[0] has been created
        quizService.create(
          testQuizs[1].tittel, 
          testQuizs[1].kategori, 
          testQuizs[1].spm, 
          testQuizs[1].a, 
          testQuizs[1].b, 
          testQuizs[1].c, 
          testQuizs[1].riktig_svar
          )
        ) 
      .then(() => // Create quizTask[2] after quizTask[1] has been created
        quizService.create(
          testQuizs[2].tittel, 
          testQuizs[2].kategori, 
          testQuizs[2].spm, 
          testQuizs[2].a, 
          testQuizs[2].b, 
          testQuizs[2].c, 
          testQuizs[2].riktig_svar
          )
        ) 
      .then(() => // Create quizTask[3] after quizTask[2] has been created
       quizService.create(
         testQuizs[3].tittel, 
         testQuizs[3].kategori, 
         testQuizs[3].spm, 
         testQuizs[3].a, 
         testQuizs[3].b, 
         testQuizs[3].c, 
         testQuizs[3].riktig_svar
        )
      ) 
      .then(() => done()); // Call done() after quizTask[2] has been created

  });
});

// Stop web server and close connection to MySQL server
afterAll((done) => {
  if (!webServer) return done.fail(new Error());
  webServer.close(() => pool.end(() => done()));
});

describe('Hent quizer (GET)', () => {
  test('Hent quizer historie (200 OK)', (done) => {
    axios.get<Quiz[]>('/historie').then((response) => {
      expect(response.status).toEqual(200);
      expect(response.data).toEqual([
        { 
          tittel: 'Testquiz1',
        }
      ]);
      done();
    });
  });

  test('Hent quizer sport (200 OK)', (done) => {
    axios.get<Quiz[]>('/sport').then((response) => {
      expect(response.status).toEqual(200);
      expect(response.data).toEqual([
        { 
          tittel: 'Testquiz2',
        }
      ]);
      done();
    });
  });

  test('Hent quizer natur og vitenskap (200 OK)', (done) => {
    axios.get<Quiz[]>('/naturogvitenskap').then((response) => {
      expect(response.status).toEqual(200);
      expect(response.data).toEqual([
        { 
          tittel: 'Testquiz4',
        }
      ]);
      done();
    });
  });
    
  test('Hent quizer samfunn og dagligliv (200 OK)', (done) => {
    axios.get<Quiz[]>('/samfunnogdagligliv').then((response) => {
      expect(response.status).toEqual(200);
      expect(response.data).toEqual([
        { 
          tittel: 'Testquiz3',
        }
      ]);
      done();
    });
  });
  
  test('Hent en quiz (200 OK)', (done) => {
    axios.get<Quiz>('/quiz/Testquiz1/spm').then((response) => {
      expect(response.status).toEqual(200);
      expect(response.data[0]).toEqual(testQuizs[0]);
      done();
    });
  });

  test('Hent alle quizer (200 OK)', (done) => {
    axios.get<Quiz[]>('/').then((response) => {
      expect(response.status).toEqual(200);
      expect(response.data).toEqual([
        { tittel: 'Testquiz1'},
        { tittel: 'Testquiz2'},
        { tittel: 'Testquiz3'},
        { tittel: 'Testquiz4'},
      ]);
      done();
    });
  });

});


describe('Hent spm (GET)', () => {
  test('Hent et spm (200 OK)', (done) => {
    axios.get<Quiz>('/rediger/2/spm').then((response) => {
      expect(response.status).toEqual(200);
      expect(response.data).toEqual(testQuizs[1]);
      done();
    });
  });

  test('Hent alle spm til quiz (200 OK)', (done) => {
    axios.get<Quiz>('/rediger/Testquiz1/alleSpm').then((response) => {
      expect(response.status).toEqual(200);
      expect(response.data[0]).toEqual(testQuizs[0]);done();
    });
  });
});


describe('Opprett ny quiz (POST)', () => {
  test('Opprett ny quiz (200 OK)', (done) => {
    axios
    .post<{}, number>('/quizny', { tittel: 'Nyquiz1', kategori: 'Sport', spm: 'Nyttspm', a: 'J', b: 'N', c: 'V', riktig_svar: 'a' })
    .then((response) => {
      expect(response.status).toEqual(200);
      expect(response.data).toEqual({ id: 5 });
      done();
    });
  });

  test('Opprett ny quiz (400)', (done) => {
    axios
    .post<{}, number>('/quizny', {  
      tittel: 'Nyquiz2',
      kategori: 'Sport', 
      spm: 'Nyttspm', 
      a: 'J', 
      b: 'N', 
      c: '', 
      riktig_svar: '', 
    })
    .then((response) => done.fail(new Error()))
    .catch((error: Error) => {
      expect(error.message).toEqual('Request failed with status code 400');
      done();
    });
  });
});


describe('Slett quiz (DELETE)', () => {
  test('Slett quiz (200 OK)', (done) => {
    axios.delete('/rediger/Testquiz3/alleSpm').then((response) => {
      expect(response.status).toEqual(200);
      done();
    });
  });

  test('Slette hele quiz (500 error)', (done) => {
    axios
      .delete<Quiz>('/rediger/Testquiz33/alleSpm')
      .then((response) => done.fail(new Error()))
      .catch((error: Error) => {
        expect(error.message).toEqual('Request failed with status code 500');
        done();
      });
  });

  test('Slett spm i quiz (200 OK)', (done) => {
    axios.delete('/rediger/4/spm').then((response) => {
      expect(response.status).toEqual(200);
      done();
    });
  });
});


describe('Oppdater quiz (PUT)', () => {
  test('Oppdater quiz (200 OK)', (done) => {
    axios
    .put<{}, void>('/quiz/1', {
      id: 1,
      tittel: 'Oppdatert tittel',
      spm: 'Oppdatert spm',
      a: 'oa',
      b: 'ob',
      c: 'oc', 
      riktig_svar: 'a',
    })
    .then((response) => {
      expect(response.status).toEqual(200);
      done();
    });
  });
}); 